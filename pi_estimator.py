import numpy as np

def estimate_pi(N):
    """

    :param N: 

    """
    count = 0
    for i in range(N):
        x, y = np.random.rand(2)
        if x**2 + y**2 < 1:
            count +=1
    return count / N * 4

print("pi estimate:", estimate_pi(10000))
